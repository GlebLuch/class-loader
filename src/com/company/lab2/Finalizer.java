package com.company.lab2;

public class Finalizer {

    Object object;

    public Finalizer(Object object) {
        this.object = object;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Object deleted");
    }
}
