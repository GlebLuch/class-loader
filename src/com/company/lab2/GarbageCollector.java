package com.company.lab2;

public class GarbageCollector {
    public static void main(String[] args) {
        Finalizer a = new Finalizer(100);
        Finalizer b = new Finalizer(100L);
        Finalizer c = new Finalizer("100");
        a = null;
        a = c;
        c = b;
        b = c; // Line7
        System.gc();
    }
}

