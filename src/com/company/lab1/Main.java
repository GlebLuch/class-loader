package com.company.lab1;

public class Main {

    public static void main(String[] args) {

        MyClassLoader myClassLoader = new MyClassLoader();
        Class<?> bytes = myClassLoader.findClass(HelloWorld.class.getName());
        System.out.println(bytes);
    }
}
